# Animated CSS Loader

Simple, animated loading circle.

* Modify settings section of style.scss file
* Compile with Sass to CSS file
* add generated CSS to your HTML file
* add `<span class="loader"></span>` to your HTML file
* DONE!